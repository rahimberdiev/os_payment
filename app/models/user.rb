class User < ApplicationRecord
  has_many :userCourses
  has_many :payments
end
