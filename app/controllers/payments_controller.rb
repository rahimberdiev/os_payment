class PaymentsController < ApplicationController
  def react 
    if params[:command]=='check'
      user = User.find_by(username: params[:account])
      
      message = user ? {
        result: '0',
        comment: 'OK',
        fields:{
          field1: user.fullname
        }
      } : {
        result: '5',
        comment: '403',
      }
      render plain: message.to_xml(root: :response) 
      return
    end
    if params[:command]=='pay'
      user = User.find_by(username: params[:account])
      if user
        payment = Payment.create(user_id:user.id,account:params[:account],summa:params[:sum].to_d,txn_id:params[:txn_id],txn_date: params[:txn_date])


        if user.payments.sum(:summa)>=480
          Subject.where(lang: user.lang).each do |subject|
            subject.courses.each do |course|
              user_course = user.userCourses.new
              user_course.course_id=course.id
              user_course.save
            end
          end
          user.full_access = true
          user.save
        end


        message = {
          result: '0',
          osmp_txn_id:  params[:txn_id],
          prv_txn: payment.id.to_s,
          sum: params[:sum],
          comment: 'OK'
        }
      else 
        message = {
          result: '5',
          osmp_txn_id:  params[:txn_id],
          sum: params[:sum],
          comment: '403'
        }
      end
      render plain: message.to_xml(root: :response).gsub('osmp-txn-id','osmp_txn_id').gsub('prv-txn','prv_txn') 
      return
    end
  end
end 
